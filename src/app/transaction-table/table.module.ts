import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ResponsiveTableComponent} from './responsive-table/responsive-table.component';
// tslint:disable-next-line:import-spacing
import {MaterialModule} from '../material/material.module';


const routes: Routes = [
    {path: '', component: ResponsiveTableComponent}
];

@NgModule({
  declarations: [
    ResponsiveTableComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class TableModule {
  constructor(){
    console.log('tableModule');
  }
}
