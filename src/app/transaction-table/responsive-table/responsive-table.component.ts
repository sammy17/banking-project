import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FirebaseService } from 'src/app/firebase.service';


@Component({
  selector: 'app-responsive-table',
  templateUrl: './responsive-table.component.html',
  styleUrls: ['./responsive-table.component.scss']
})
export class ResponsiveTableComponent implements OnInit {
  displayedColumns = ['No', 'Account-No', 'Name', 'Type', 'Balance', 'Transcation-Type', 'Amount', 'Current-Balance'];
  dataSource: any;
  mySub: Subscription = new Subscription();
  users: any[] = [];


  constructor(private postService: FirebaseService) { }

  ngOnInit(): void {
    this.postService.transaction().subscribe(val => {
      this.dataSource = val;
    });
    this.postService.lastTransaction().subscribe(res => {
      this.users.push(res);
    });
  }
}


