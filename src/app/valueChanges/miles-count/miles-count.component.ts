import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-miles-count',
  templateUrl: './miles-count.component.html',
  styleUrls: ['./miles-count.component.scss']
})
export class MilesCountComponent implements OnInit {
  toppingList = [];
  calculateForm: FormGroup;
  // tslint:disable-next-line:new-parens
  mySub: Subscription = new Subscription;
  // tslint:disable-next-line:new-parens
  mySub1: Subscription = new Subscription;
  // tslint:disable-next-line:new-parens
  mySub2: Subscription = new Subscription;
  // tslint:disable-next-line:new-parens
  mySub3: Subscription = new Subscription;

  constructor() { }
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): void {
    this.mySub.unsubscribe();
    this.mySub1.unsubscribe();
    this.mySub2.unsubscribe();
    this.mySub3.unsubscribe();
  }

  ngOnInit(): void {
    this.valueChangesForm();

    this.mySub.add
    (this.calculateForm.get('totalRate').valueChanges.subscribe
    (value => {
      this.valueChangeRatePerMile();
      this.valueChangeEarnings();
    }));

    this.mySub1.add
    (this.calculateForm.get('ratePerMile').valueChanges.subscribe
    (value => {
      this.valueChangeTotalRate();
    }));

    this.mySub2.add(this.calculateForm.get('totalMiles').valueChanges.subscribe
    (value => {
      this.valueChangeEarnings();
      this.valueChangeTotalRate();
    }));

    this.mySub3.add
    (this.calculateForm.get('pay').valueChanges.subscribe
    (value => {
      this.calculateTotalPay();
    }));
  }
  // tslint:disable-next-line:typedef
 valueChangesForm() {
    this.calculateForm = new FormGroup({
      totalRate: new FormControl(''),
      ratePerMile: new FormControl(''),
      totalMiles: new FormControl(''),
      earnings: new FormControl
      ({ value: '',
        disabled: true }),
      expensive: new FormControl
      ({ value: '17',
        disabled: true }),
      pay: new FormControl(''),
      total: new FormControl({ value: '',
        disabled: true }),
    });
  }
  // tslint:disable-next-line:typedef
  valueChangeTotalRate() {
    const newTotalRate = this.calculateForm.controls.totalRate.value;
    const newRatePerMile = this.calculateForm.controls.ratePerMile.value;
    const newTotalMiles = this.calculateForm.controls.totalMiles.value;
    let newValue;

    if (newRatePerMile > 0 && newTotalMiles > 0) {
      newValue = newRatePerMile * newTotalMiles;
    }
    else {
      newValue = null;
    }
    // tslint:disable-next-line:triple-equals
    if (newValue != newTotalRate) {
      this.calculateForm.controls.totalRate.patchValue(newValue);
      this.calculateForm.controls.totalRate.markAsDirty();
    }
  }
  // tslint:disable-next-line:typedef
  valueChangeRatePerMile() {

    const newTotalRate = this.calculateForm.controls.totalRate.value;
    const newRatePerMile = this.calculateForm.controls.ratePerMile.value;
    const newTotalMiles = this.calculateForm.controls.totalMiles.value;
    let newValue;

    if (newTotalRate > 0 && newTotalMiles > 0) {
      newValue = newTotalRate / newTotalMiles;
    }
    else {
      newValue = null;
    }
    // tslint:disable-next-line:triple-equals
    if (newValue != newRatePerMile) {
      this.calculateForm.controls.ratePerMile.patchValue(newValue);
      this.calculateForm.controls.ratePerMile.markAsDirty();
    }
  }
  // tslint:disable-next-line:typedef
  valueChangeEarnings() {
    const newTotalRate = this.calculateForm.controls.totalRate.value;
    const newTotalMiles = this.calculateForm.controls.totalMiles.value;
    const newExpensive = this.calculateForm.controls.expensive.value;
    const newEarnings = this.calculateForm.controls.earnings.value;
    let newValue;

    if (newTotalRate > 0 && newTotalMiles > 0) {
      newValue = newTotalRate - (newTotalMiles * newExpensive);
    }
    else {
      newValue = null;
    }
    // tslint:disable-next-line:triple-equals
    if (newValue != newEarnings) {
      this.calculateForm.controls.earnings.patchValue(newValue);
      this.calculateForm.controls.earnings.markAsDirty();
    }
  }
  // tslint:disable-next-line:typedef
  calculateTotalPay() {
    const newEarnings = this.calculateForm.controls.earnings.value;
    const newPay = this.calculateForm.controls.pay.value;
    const totalPay = this.calculateForm.controls.total.value;
    let newValue;
    if (newEarnings > 0 && newPay > 0) {
      newValue = newEarnings - newPay;
    }
    else {
      newValue = null;
    }
    // tslint:disable-next-line:triple-equals
    if (newValue != newEarnings) {
      this.calculateForm.controls.total.patchValue(newValue);
      this.calculateForm.controls.total.markAsDirty();
    }
  }
}



