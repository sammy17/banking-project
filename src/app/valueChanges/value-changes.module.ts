import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '../material/material.module';
import { MilesCountComponent } from './miles-count/miles-count.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';


const routes: Routes = [
  {path: '', component: MilesCountComponent}
];

@NgModule({
  declarations: [
    MilesCountComponent
  ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ]
})
export class ValueChangesModule {
  constructor() {
    console.log('this.ValueChangesModule');
  }
}
