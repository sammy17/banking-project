import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './bank-details/login-page/login-page.component';
import { DialogBoxLoginComponent } from './bank-details/dialog-box-login/dialog-box-login.component';
import { BankFormComponent } from './bank-details/bank-form/bank-form.component';
import { MilesCountComponent } from './valueChanges/miles-count/miles-count.component';
import { TableModule } from './transaction-table/table.module';
import {ValueChangesModule} from './valueChanges/value-changes.module';


const routes: Routes = [
  { path: '',
    pathMatch: 'full',
    redirectTo: 'login' },

  { path: 'login',
    component: LoginPageComponent },

  { path: 'update/id',
    component: DialogBoxLoginComponent },

  { path: 'bankForm/:id',
    component: BankFormComponent },
  // {path: 'transaction', loadChildren:'src/app/transaction-table/table.module#TableModule'}
  {
    path: 'transaction',
    loadChildren: () =>
      import('src/app/transaction-table/table.module').then(module => module.TableModule)
  },

  {
    path: 'voluechange',
    loadChildren: () =>
      import('src/app/valueChanges/value-changes.module').then(module => module.ValueChangesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
