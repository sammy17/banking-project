import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alert-dialog-box',
  templateUrl: './alert-dialog-box.component.html'
})
export class AlertDialogBoxComponent implements OnInit {
  message = 'Transaction done....';
  confirmButtonText = 'ok';
  cancelButtonText = '';

  constructor( private dialogRef: MatDialogRef<AlertDialogBoxComponent>,
               private router: Router) { }

  ngOnInit(): void {
  }
  onConfirmClick(): void {
    this.dialogRef.close(true);
    this.router.navigate(['/login']);
    //  setTimeout(() => {
    //    this.router.navigate(['/login']);
    // }, 2000);
  }
}
