import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { FirebaseService } from 'src/app/firebase.service';
import { AlertDialogBoxComponent } from '../alert-dialog-box/alert-dialog-box.component';

@Component({
  selector: 'app-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.scss']
})
export class BankFormComponent implements OnInit {
  bankDetailsForm: FormGroup;
  users: any[] = [];
  findUsers: any[] = [];
  UserDetails: any;
  types: string[] = ['credit', 'debit'];
  alert: MatDialogRef<AlertDialogBoxComponent>;
  paramValue: string;
  // users: Data = {accountNo: '', name: '', type: '', currentBalance: ''};
  // transactionFirebase: Data = { accountNo: '', name: '', type: '', currentBalance: '', amount: '', transactionType: '', balance: '' };
  // fileNameDialogRef: MatDialogRef<UpdateAccountDetailsComponent>;
  // @Output() accountNoId = new EventEmitter<any>();

  constructor(private formBuilder: FormBuilder,
              private firebaseService: FirebaseService,
              private http: HttpClient,
              private dialog: MatDialog,
              private activatedRoute: ActivatedRoute,
              private router: Router ) { }

  ngOnInit(): void {
    // params (activated-route);
    this.paramValue = this.activatedRoute.snapshot.paramMap.get('id');
    // form function call;
    this.createForm();
    // simple api call (!rxjs),update account details;#get
    this.firebaseService.getUpdateUserAccountDetails().subscribe(response => {
      this.UserDetails = response;
      console.log(this.UserDetails);
      this.addUser();
    });
    // this.firebaseService.transaction().subscribe(data => {
    //   this.users = data;
    //   console.log(this.users);
    // });
    // setTimeout(() => {
    //   alert('Time-Up');
    //   this.router.navigate(['/login']);
    // }, 100000);

  }
  // form;
  // tslint:disable-next-line:typedef
  createForm = () => {
    this.bankDetailsForm = this.formBuilder.group({
      accountNo: [({
        value: '',
        disabled: true })],
      name: [({
        value: '',
        disabled: true })],
      type: [({
        value: '',
        disabled: true })], // [Validators.required]
      openingBalance: [({
        value: '', disabled: true })],
      amount: [''],
      transactionType: [''],
      balance: [({
        value: '',
        disabled: true })],
    });
  }
// find values and set values
  // tslint:disable-next-line:typedef
  addUser() {
    this.findUsers = this.UserDetails;
    const search = this.findUsers.find(element => element.accountNo === this.paramValue);
    if (search) {
      this.bankDetailsForm.patchValue({
        accountNo: search.accountNo,
        name: search.name,
        type: search.type,
        openingBalance: search.currentBalance,
      });
    }
    else {
      alert('account.no.invalid');
    }

    const accountType = this.bankDetailsForm.controls.type.value;
    if (accountType === 'Car Loan') {
      this.types = ['credit'];
    }
    else {
      this.types = ['credit', 'debit'];
    }
  }
// tslint:disable-next-line:typedef
  valueChangesBalance = () => {
    const userOpeningBalance = this.bankDetailsForm.controls.openingBalance.value;
    const userAmount = this.bankDetailsForm.controls.amount.value;
    const transaction = this.bankDetailsForm.controls.transactionType.value;
    const accountType = this.bankDetailsForm.controls.type.value;
    let newValue = 0;

    if (userOpeningBalance && userAmount && transaction === 'credit' && accountType === 'Saving') {
      newValue = (+userOpeningBalance) + (+userAmount);
    }
    else if (userOpeningBalance && userAmount && transaction === 'credit' && accountType === 'Car Loan') {
      newValue = (+userOpeningBalance) - (+userAmount);
    }
    else if (userOpeningBalance && userAmount && transaction === 'debit') {
      if ((+userOpeningBalance) < (+userAmount)) {
        alert('Insufficient Balance.');
        this.bankDetailsForm.controls.amount.patchValue(0);
        this.bankDetailsForm.controls.transactionType.patchValue('');
      }
      else {
        newValue = userOpeningBalance - userAmount;
      }
    }
    this.bankDetailsForm.controls.balance.patchValue(newValue);
    this.bankDetailsForm.controls.balance.markAsDirty();

  }

  // tslint:disable-next-line:typedef
  // button = () => {
  //
  //   this.transactionFirebase.accountNo = this.bankDetailsForm.controls.accountNo.value;
  //   this.transactionFirebase.name = this.bankDetailsForm.controls.name.value;
  //   this.transactionFirebase.type = this.bankDetailsForm.controls.type.value;
  //   this.transactionFirebase.currentBalance = this.bankDetailsForm.controls.openingBalance.value;
  //   this.transactionFirebase.amount = this.bankDetailsForm.controls.amount.value;
  //   this.transactionFirebase.transactionType = this.bankDetailsForm.controls.transactionType.value;
  //   this.transactionFirebase.balance = this.bankDetailsForm.controls.balance.value;
  //   const data = JSON.stringify(this.transactionFirebase);
  //   const parse1 = JSON.parse(data);
  //   this.firebaseService.sendTransaction(parse1);
  //   this.bankDetailsForm.reset();
  //   console.log(parse1);
  // }

  // tslint:disable-next-line:typedef
  // updateDate(){
  //   this.users.accountNo = this.bankDetailsForm.get('accountNo')?.value;
  //   this.users.name = this.bankDetailsForm.get('name')?.value;
  //   this.users.type = this.bankDetailsForm.get('type')?.value;
  //   this.users.currentBalance = this.bankDetailsForm.get('balance')?.value;
  //   // this.users.amount = this.bankDetailsForm.get('amount')?.value;
  //   // this.users.transactionType =this.bankDetailsForm.get('transactionType')?.value;
  //   // this.users.balance =this.bankDetailsForm.get('balance')?.value;
  //   // tslint:disable-next-line:radix
  //   const id = parseInt(this.users.accountNo);
  //   const stringify = JSON.stringify(this.users);
  //   const parse = JSON.parse(stringify);
  //   this.firebaseService.updateBalanceDetails(id, parse).subscribe(data => {
  //     console.log(data);
  //    });
  // }
  // tslint:disable-next-line:typedef
  openCreateAccount() {
    this.alert = this.dialog.open(AlertDialogBoxComponent, {
      minHeight: '100px',
      minWidth: '300px',
    });
  }
  // tslint:disable-next-line:typedef
  onSubmit() {
    console.log(this.bankDetailsForm);
    this.button();
    setTimeout(() => {
      this.openCreateAccount();
      this.router.navigate(['/login']);
    }, 8000);
  }
// push values (table)
  // tslint:disable-next-line:typedef
  button = () => {
    const userId = this.bankDetailsForm.controls.accountNo.value;
    const userName = this.bankDetailsForm.controls.name.value;
    const userCardType = this.bankDetailsForm.controls.type.value;
    const userOpeningBalance = this.bankDetailsForm.controls.openingBalance.value;
    const userAmount = this.bankDetailsForm.controls.amount.value;
    const userTransactionType = this.bankDetailsForm.controls.transactionType.value;
    const userBalance = this.bankDetailsForm.controls.balance.value;

    if (this.bankDetailsForm.invalid) {
      return false;
    } else {
      this.users.push({
        accountNo: userId,
        name: userName,
        type: userCardType,
        openingBalance: userOpeningBalance,
        amount: userAmount,
        transactionType: userTransactionType,
        balance: userBalance,
      });
    }
    this.bankDetailsForm.reset();
  };
}
