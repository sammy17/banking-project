import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FirebaseService } from 'src/app/firebase.service';
import { DialogBoxCreateComponent } from '../dialog-box-create/dialog-box-create.component';
import { DialogBoxLoginComponent } from '../dialog-box-login/dialog-box-login.component';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  loginAccount: MatDialogRef<DialogBoxLoginComponent>;
  createAccount: MatDialogRef<DialogBoxCreateComponent>;
  firebase;

  constructor(private dialog: MatDialog,
              private postService: FirebaseService) { }

  ngOnInit(): void {
  }
  // tslint:disable-next-line:typedef
  openAddFileDialog() {
    this.loginAccount = this.dialog.open(DialogBoxLoginComponent, {
      minHeight: '80px',
      minWidth: '300px',

    });
  }
  // tslint:disable-next-line:typedef
  openCreateAccount() {
    this.createAccount = this.dialog.open(DialogBoxCreateComponent, {
      minHeight: '200px',
      minWidth: '300px',

    });
  }

}
