import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import {FirebaseService} from 'src/app/firebase.service';

@Component({
  selector: 'app-dialog-box-create',
  templateUrl: './dialog-box-create.component.html',
  styleUrls: ['./dialog-box-create.component.scss']
})
export class DialogBoxCreateComponent implements OnInit {
  signupForm: FormGroup;
  account;
  firebase: any;
  i: any;
  pass: any;
  createForm: FormGroup;



  constructor(    private dialogRef: MatDialogRef<DialogBoxCreateComponent>,
                  private router: Router,
                  private postService: FirebaseService) { }

  ngOnInit(): void {
    this.form();
    // this.postService.getbank().subscribe(data => {
    //   this.firebase = data['-MhON0ouwaAnr5CcuY7H'];
    // });
    this.account = this.signupForm.controls.accountNo.value;
    this.pass = this.signupForm.controls.password.value;
  }
  // tslint:disable-next-line:typedef
  form(){
    this.createForm = new FormGroup({
      accountNo: new FormControl(''),
      currentBalance: new FormControl(''),
      name: new FormControl(''),
      password: new FormControl(''),
      type: new FormControl('')
    });
    // this.postService.bankDetails();
  }
  //
  // apiData(){
  //   this.account = this.signupForm.controls['accountNo'].value;
  //   this.pass = this.signupForm.controls['password'].value;
  //   this.i = this.firebase;
  //
  //   let search = this.i.find(element => element.accountNo === this.account && element.password === this.pass);
  //   if (search) {
  //     this.router.navigate(['/bankForm/'+ this.account]);
  //     this.dialogRef.close();
  //
  //   }
  //   else
  //    alert("account.no.invalid");
  //    this.dialogRef.close();
  // }
  // onCloseDialog() {
  //   this.dialogRef.close();
  // }
  // tslint:disable-next-line:typedef
  onSubmit(){
    this.account = this.signupForm.controls.accountNo.value


    this.router.navigate(['/bankForm/' + this.account]);
    this.dialogRef.close();
  }

}

