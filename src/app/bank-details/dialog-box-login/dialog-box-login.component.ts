import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FirebaseService } from 'src/app/firebase.service';

@Component({
  selector: 'app-dialog-box-login',
  templateUrl: './dialog-box-login.component.html',
  styleUrls: ['./dialog-box-login.component.scss']
})
export class DialogBoxLoginComponent implements OnInit {

  signupForm: FormGroup;
  account;
  firebase: any;
  findUser: any;
  pass: any;

  constructor(private dialogRef: MatDialogRef<DialogBoxLoginComponent>,
              private router: Router,
              private postService: FirebaseService) { }

  ngOnInit(): void {

    this.form();

    this.postService.getUsersAccountsPasswords().subscribe(data => {
      this.firebase = data;
      console.log(this.firebase);
    });
    this.account = this.signupForm.controls.accountNo.value;
    this.pass = this.signupForm.controls.password.value;
  }

  // tslint:disable-next-line:typedef
  form() {
    this.signupForm = new FormGroup({
      accountNo: new FormControl(''),
      password: new FormControl('')
    });
  }

  // tslint:disable-next-line:typedef
  apiData() {
    this.account = this.signupForm.controls.accountNo.value;
    this.pass = this.signupForm.controls.password.value;
    this.findUser = this.firebase;

    const search = this.findUser.find(element => element.accountNo === this.account && element.password === this.pass);
    if (search) {
      this.router.navigate(['/bankForm/' + this.account]);
      this.dialogRef.close();
    }
    else {
      alert('account.no.invalid');
    }
    this.dialogRef.close();
  }
  // onCloseDialog() {
  //   this.dialogRef.close();
  // }

  // tslint:disable-next-line:typedef
  onSubmit() {
    console.log(this.signupForm);
  }

}

