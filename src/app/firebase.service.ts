import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import accountmockdata from 'src/app/mockdata/accountdetailsmockdata.json';
@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
urlUpdateUserAccountDetails = 'https://base-tutorial-default-rtdb.firebaseio.com/data/-MiJEq1ldWgm8vkHDol2.json';
urlUsersAccountsPasswords = 'https://base-tutorial-default-rtdb.firebaseio.com/data/-Mh_3ixNrttX87TjKrEC.json';
  constructor(private http: HttpClient) { }
  // tslint:disable-next-line:typedef
  getUsersAccountsPasswords(){
    const url = this.urlUsersAccountsPasswords;
    return this.http.get<string>(url);
  }
  // tslint:disable-next-line:typedef
  getUpdateUserAccountDetails() {
    const url = this.urlUpdateUserAccountDetails;
    return this.http.get<string>(url);
  }
  // tslint:disable-next-line:typedef
  updateBalanceDetails(id, formData) {
    const url = 'https://base-tutorial-default-rtdb.firebaseio.com/data/-Mh_W2533uemswYDBJmI/' + id + '.json';
    return this.http.put<string>(url, formData);
  }

  // tslint:disable-next-line:typedef
  lastTransaction(){
    return this.http.get('https://base-tutorial-default-rtdb.firebaseio.com/data/-Mh_47gtyhePvWpQ4Jro.json');
  }
  // tslint:disable-next-line:typedef
  transaction() {
   return this.http.get('https://base-tutorial-default-rtdb.firebaseio.com/data/-Mh_EyjcP_HgjQzZuE19.json');
  }

  // tslint:disable-next-line:typedef
  sendTransaction(data) {
   return this.http.put('https://base-tutorial-default-rtdb.firebaseio.com/data/-Mh_47gtyhePvWpQ4Jro.json', data);
  }

  getPostData(): Observable<string> {
      // if (environment.postmockdata)
      //   return of(postmockdata);
      // else {
        const url = 'https://jsonplaceholder.typicode.com/users';
        return this.http.get<string>(url);
    }

  // bankDetails(){
  //    const data = accountmockdata;
  //    this.http.post('https://base-tutorial-default-rtdb.firebaseio.com/data.json', data).subscribe(val => {
  //     console.log(val);
  //   });
  // }
}

