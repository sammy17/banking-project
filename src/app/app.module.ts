import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginPageComponent } from './bank-details/login-page/login-page.component';
import { DialogBoxLoginComponent } from './bank-details/dialog-box-login/dialog-box-login.component';
import { DialogBoxCreateComponent } from './bank-details/dialog-box-create/dialog-box-create.component';
import { HeaderComponent } from './header/header.component';
import { MaterialModule} from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BankFormComponent } from './bank-details/bank-form/bank-form.component';
import { HttpClientModule } from '@angular/common/http';
import { AlertDialogBoxComponent } from './bank-details/alert-dialog-box/alert-dialog-box.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DialogBoxLoginComponent,
    DialogBoxCreateComponent,
    HeaderComponent,
    BankFormComponent,
    AlertDialogBoxComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(){
    console.log('app module');
  }
}
